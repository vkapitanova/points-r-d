package points.repository;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import points.model.Point;

import java.util.List;

@Repository
public class PointDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @SuppressWarnings("unchecked")
    public List<Point> list() {
        return (List<Point>)sessionFactory.getCurrentSession().createQuery("from Point").list();
    }

    @Transactional
    public Point get(long id) {
        return (Point)sessionFactory.getCurrentSession().createQuery("from Point where id="+id).uniqueResult();
    }

    @Transactional
    public void add(Point point) {
        sessionFactory.getCurrentSession().save(point);
    }

    @Transactional
    public void delete(long pointId) {
        sessionFactory.getCurrentSession().delete(get(pointId));
    }

    @Transactional
    public void deleteAll() {
        sessionFactory.getCurrentSession().createQuery("delete from Point").executeUpdate();
    }
}
