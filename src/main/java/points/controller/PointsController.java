package points.controller;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import points.service.PointService;
import points.model.Point;

import java.util.List;

@Controller
@RequestMapping("points")
@Api(value = "Points", description = "Operations with points")
public class PointsController {
    @Autowired
    PointService pointService;

    @ApiOperation(value = "Points list", notes = "All points ever created")
    @RequestMapping(value = {"list"}, method = RequestMethod.GET)
    public @ResponseBody List<Point> points() {
        return pointService.list();
    }

    @ApiOperation(value = "Get point", notes = "Get specific point")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public @ResponseBody Point show(
            @ApiParam(value = "Identifier of point", required = true)
            @PathVariable("id") long id
    ) {
        return pointService.get(id);
    }

    @ApiOperation(value = "Add point", notes = "Add new point")
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public @ResponseBody Point add(@RequestBody Point point) {
        pointService.add(point);
        return point;
    }

    @ApiOperation(value = "Delete point", notes = "Delete specific point")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(
            @ApiParam(value = "Identifier of point", required = true)
            @PathVariable("id") long pointId
    ) {
        pointService.delete(pointId);
    }

    @ApiOperation(value = "Delete all points", notes = "Delete all points ever created")
    @RequestMapping(value = "deleteall", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteAll() {
        pointService.deleteAll();
    }
}
