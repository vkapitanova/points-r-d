package points.model;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@ApiModel(value = "Point", description = "Point on map" )
@Table( name = "points")
public class Point {
    @Id
    @GeneratedValue(generator="points_id_generator")
    @GenericGenerator(name="points_id_generator", strategy = "increment")
    @ApiModelProperty(value = "Identifier", required = true)
    private Long id;

    @Column(nullable = false)
    @ApiModelProperty(value = "Name", required = true)
    private String name;

    @Column(nullable = false)
    @ApiModelProperty(value = "Address", required = true)
    private String address;

    @Column(nullable = false)
    @ApiModelProperty(value = "Latitude", required = true)
    private Double lat;

    @ApiModelProperty(value = "Longitude", required = true)
    @Column(nullable = false)
    private Double lon;

    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
