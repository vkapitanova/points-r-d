var pointsControllers = angular.module('pointsControllers', []);

pointsControllers.controller('PointShowController', ['$scope', '$routeParams', '$http', '$location', 'Point',
    function($scope, $routeParams, $http, $location, Point) {
        $scope.pointId = $routeParams.pointId;

        Point.get({pointId: $scope.pointId}, function(point) {
            $scope.point = point;
            new Map([point]);
        });

        $scope.deletePoint = function() {
            Point.delete({pointId: $scope.pointId}, function() {
                $location.path('/points');
            });
        }
    }
]);

pointsControllers.controller('PointsListController', ['$scope', '$http', '$route', 'Point',
    function ($scope, $http, $route, Point) {
        Point.list({}, function(points) {
            $scope.points = points;
            $scope.map = new Map(points);
        });

        $scope.deleteAllPoints = function() {
            Point.deleteAll({}, function() {
                $route.reload();
            });
        }
    }
]);

pointsControllers.controller('PointsAddController', ['$scope', '$http', 'Point',
    function($scope, $http, Point) {
        $scope.point = {};

        $scope.createPoint = function() {
            var form = $('#add_point_form');
            var name = $('.name-group input', form).val();
            $('.name-group .error_list').empty();
            $('.address-group .error_list').empty();
            if (name == "") {
                $('#errorTmpl').tmpl([{error: "Введите, пожалуйста, название точки"}]).appendTo('.name-group .error_list');
            }
            var address = $('.address-group input', form).val();
            $scope.map.findAddress(address, {
                success: function(location) {
                    $.extend($scope.point, location);
                    Point.add({}, $scope.point, function(point) {
                        $scope.points.push(point);
                        $scope.map.addMarkers([point])
                    });
                },
                fail: function() {
                    $('#errorTmpl').tmpl([{error: "Введите, пожалуйста, корректный адрес"}]).appendTo('.address-group .error_list');
                }
            });
        };
    }
]);
