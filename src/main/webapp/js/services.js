var pointsServices = angular.module('pointsServices', ['ngResource']);

pointsServices.factory('Point', ['$resource',
    function($resource){
        return $resource('/points/points/:pointId.json', {}, {
            list: {method:'GET', params:{pointId: 'list'}, isArray:true},
            add: {method:'POST', params:{pointId: 'add'}, headers: {'Content-Type': 'application/json'}},
            deleteAll: {method:'DELETE', params:{pointId: 'deleteall'}}
        });
    }]
);