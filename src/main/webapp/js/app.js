var pointsApp = angular.module('pointsApp', [
    'ngRoute',
    'pointsControllers',
    'pointsServices'
]);

pointsApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/points', {
                templateUrl: 'list.html',
                controller: 'PointsListController'
            }).
            when('/show/:pointId', {
                templateUrl: 'show.html',
                controller: 'PointShowController'
            }).
            otherwise({
                redirectTo: '/points'
            });
    }]);
