function Map(points) {
    var self = this;
    init();

    function init() {
        self.geocoder = new google.maps.Geocoder();
        self.markers = [];
        self.map = createMap();
        self.addMarkers(points);
    }

    function createMap() {
        var myOptions = {
            zoom: 8,
            center: new google.maps.LatLng(55.7522200, 37.6155600),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        return new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    }
}

Map.prototype.addMarkers = function(points) {
    var self = this;
    $(points).each(function(i, point) {
        self.markers.push(new google.maps.Marker({
            position: new google.maps.LatLng(point.lat, point.lon),
            map: self.map,
            title: point.name
        }));
    });
    if (points.length > 0) {
        this.zoomMap();
    }
};

Map.prototype.zoomMap = function() {
    var self = this;
    var bounds = new google.maps.LatLngBounds();
    for (var i in this.markers) {
        bounds.extend(this.markers[i].position);
    }
    self.map.fitBounds(bounds);
    var listener = google.maps.event.addListener(this.map, "idle", function() {
        if (self.map.getZoom() > 16) self.map.setZoom(16);
        google.maps.event.removeListener(listener);
    });
};

Map.prototype.findAddress = function(address, callbacks) {
    $.ajax({
        url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false',
        success: function(data) {
            if (data.status == 'OK') {
                if (typeof callbacks.success == 'function') {
                    var location = data.results[0];
                    var result = {
                        address: location.formatted_address,
                        lat: location.geometry.location.lat,
                        lon: location.geometry.location.lng
                    };
                    callbacks.success(result);
                }
            } else {
                if (typeof callbacks.fail == 'function')
                    callbacks.fail(data.status);
            }
        },
        dataType: 'json'
    });
}
